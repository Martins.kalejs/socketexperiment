package com.maartin.socket.common

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory

fun Context.bitmapDescriptorFromVector(): BitmapDescriptor? {
    return ContextCompat.getDrawable(this, getRandomMarkerPin())?.run {
        setBounds(0, 0, intrinsicWidth, intrinsicHeight)
        val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
        draw(Canvas(bitmap))
        BitmapDescriptorFactory.fromBitmap(bitmap)
    }
}

private fun getRandomMarkerPin(): Int {
    val availablePins = arrayListOf(
        com.maartin.socket.R.drawable.ic_map_pin_one,
        com.maartin.socket.R.drawable.ic_map_pin_two,
        com.maartin.socket.R.drawable.ic_map_pin_three,
        com.maartin.socket.R.drawable.ic_map_pin_four
    )
    val pos = (0..3).random()

    return availablePins[pos]
}