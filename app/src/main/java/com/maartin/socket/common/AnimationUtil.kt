package com.maartin.socket.common

import android.os.Handler
import android.os.SystemClock
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.SphericalUtil

object AnimationUtil {

    fun animateMarkerTo(marker: Marker, finalPosition: LatLng) {
        val startPosition = marker.position
        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val interpolator: Interpolator = AccelerateDecelerateInterpolator()
        val durationInMs = 3000f

        handler.post(object : Runnable {
            var elapsed: Long = 0
            var t = 0f
            var v = 0f

            override fun run() {
                elapsed = SystemClock.uptimeMillis() - start
                t = elapsed / durationInMs
                v = interpolator.getInterpolation(t)
                marker.position = SphericalUtil.interpolate(startPosition, finalPosition, v.toDouble())

                if (t < 1) {
                    handler.postDelayed(this, 16)
                }
            }
        })
    }

}