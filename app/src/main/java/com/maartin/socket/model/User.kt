package com.maartin.socket.model

data class User(
    var id: Int,
    var name: String,
    var imageUrl: String,
    var latitude: Double,
    var longitude: Double
)