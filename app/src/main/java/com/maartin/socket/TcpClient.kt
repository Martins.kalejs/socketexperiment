package com.maartin.socket

import java.io.*
import java.net.Socket

class TcpClient(
    private val socketStateChangeListener: OnSocketChange
) {
    private var run = false
    private var serverMessage: String? = null
    private var bufferOut: PrintWriter? = null
    private var bufferIn: BufferedReader? = null

    fun run() {
        try {
            val socket = Socket(SERVER_IP, SERVER_PORT)

            if (!run) {
                run = true
                socketStateChangeListener.onConnected()
            }

            try {
                bufferOut = PrintWriter(BufferedWriter(OutputStreamWriter(socket.getOutputStream())), true)
                bufferIn = BufferedReader(InputStreamReader(socket.getInputStream()))
                sendingAuthMessageToServer()

                while (run) {
                    serverMessage = bufferIn!!.readLine()

                    if (serverMessage != null) {
                        socketStateChangeListener.messageReceived(serverMessage ?: "")
                    }
                }
            } catch (e: Exception) {
                socketStateChangeListener.onError(e.localizedMessage ?: "")
            } finally {
                socketStateChangeListener.onError(SOCKET_ERROR_MESSAGE)
                socket.close()
            }
        } catch (e: Exception) {
            socketStateChangeListener.onError(e.localizedMessage ?: "")
        }
    }

    private fun sendingAuthMessageToServer() {
        bufferOut?.let {
            it.println(AUTH_MESSAGE)
            it.flush()
        }
    }

    fun stopClient() {
        run = false

        if (bufferOut != null) {
            bufferOut!!.flush()
            bufferOut!!.close()
        }

        bufferIn = null
        bufferOut = null
        serverMessage = null
    }

    interface OnSocketChange {
        fun messageReceived(message: String)
        fun onConnected()
        fun onError(errorMessage: String)
    }

    companion object {
        const val SERVER_IP = "ios-test.printful.lv"
        const val SERVER_PORT = 6111

        const val SOCKET_ERROR_MESSAGE = "Socket connection was lost"
        const val AUTH_MESSAGE = "AUTHORIZE test@test.com"
    }
}