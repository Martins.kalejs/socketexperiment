package com.maartin.socket.features.users

import android.content.res.Resources
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.maartin.socket.R
import com.maartin.socket.common.AnimationUtil
import com.maartin.socket.common.bitmapDescriptorFromVector
import com.maartin.socket.model.User
import es.dmoral.toasty.Toasty


class UsersActivity : AppCompatActivity(), OnMapReadyCallback {
    private var googleMap: GoogleMap? = null
    private var markerMap = HashMap<Int, Marker>()
    private val customMarkerInfoViewAdapter = CustomMarkerInfoViewAdapter(this)

    val viewModel: UsersViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppThemeMap)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_users)
        setupMap()
        setupObservables()
    }

    override fun onMapReady(map: GoogleMap?) {
        googleMap = map
        setNightMode()

        googleMap?.apply {
            setMinZoomPreference(15.0f)
            setMaxZoomPreference(20.0f)
            setInfoWindowAdapter(customMarkerInfoViewAdapter)
        }

        viewModel.runTcpClient()
    }

    private fun setupObservables() {
        viewModel.users.observe(this, {
            setupUserMarkers(it)
        })
        viewModel.userUpdates.observe(this, {
            if (it.isNotEmpty()) {
                updateUserCurrentLocation(it)
            }
        })
        viewModel.showSocketConnectionSuccessful.observe(this, {
            onSocketConnectionSuccessful()
        })
        viewModel.showSocketConnectionError.observe(this, {
            onSocketConnectionError()
        })
    }

    private fun setupUserMarkers(users: ArrayList<User>) {
        if (googleMap == null || users.isEmpty()) return

        users.forEach { user ->
            val userLocation = LatLng(user.latitude, user.longitude)
            val userIcon = bitmapDescriptorFromVector()
            val userMarkerOptions = MarkerOptions()
                .position(userLocation)
                .title(user.name)
                .icon(userIcon)

            val marker = googleMap!!.addMarker(userMarkerOptions)
            marker.tag = user

            markerMap[user.id] = marker
        }
    }

    private fun updateUserCurrentLocation(users: ArrayList<User>) {
        val builder = LatLngBounds.Builder()

        users.forEach { user ->
            val markerToBeUpdated = markerMap[user.id]
            val userLocation = LatLng(user.latitude, user.longitude)

            markerToBeUpdated?.let {
                if (it.isInfoWindowShown) {
                    it.showInfoWindow()
                }

                AnimationUtil.animateMarkerTo(it, userLocation)
            }

            builder.include(userLocation)
        }

        val bounds: LatLngBounds = builder.build()
        googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(bounds.center, 10f))
    }

    private fun setupMap() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync(this)
    }

    private fun onSocketConnectionSuccessful() {
        Toasty.success(this, getString(R.string.connection_successful), Toasty.LENGTH_SHORT).show()
    }

    private fun onSocketConnectionError() {
        Toasty.error(this, getString(R.string.connection_failed), Toasty.LENGTH_SHORT).show()
    }

    private fun setNightMode() {
        try {
            googleMap?.setMapStyle(
                MapStyleOptions.loadRawResourceStyle(this, R.raw.style_json)
            )
        } catch (e: Resources.NotFoundException) {
            Toasty.warning(this, getString(R.string.loading_night_mode_failed), Toasty.LENGTH_SHORT).show()
        }
    }

}