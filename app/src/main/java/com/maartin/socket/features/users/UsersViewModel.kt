package com.maartin.socket.features.users

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.maartin.socket.TcpClient
import com.maartin.socket.common.SingleLiveEvent
import com.maartin.socket.model.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class UsersViewModel(app: Application) : AndroidViewModel(app) {
    private val _users = MutableLiveData<ArrayList<User>>()
    val users: LiveData<ArrayList<User>>
        get() = _users
    private val _userUpdates = MutableLiveData<ArrayList<User>>()
    val userUpdates: LiveData<ArrayList<User>>
        get() = _userUpdates

    private val _showSocketConnectionError = SingleLiveEvent<Void>()
    val showSocketConnectionError: LiveData<Void>
        get() = _showSocketConnectionError
    private val _showSocketConnectionSuccessful = SingleLiveEvent<Void>()
    val showSocketConnectionSuccessful: LiveData<Void>
        get() = _showSocketConnectionSuccessful

    private val tcpMessageReceiver = object : TcpClient.OnSocketChange {
        override fun messageReceived(message: String) {
            parseReceivedMessage(message)
        }

        override fun onConnected() {
            _showSocketConnectionSuccessful.postCall()
        }

        override fun onError(errorMessage: String) {
            _showSocketConnectionError.postCall()
        }
    }
    private val tcpClient = TcpClient(tcpMessageReceiver)
    private var content = ArrayList<User>()

    fun runTcpClient() {
        viewModelScope.launch {
            withContext(Dispatchers.Default) {
                tcpClient.run()
            }
        }
    }

    private fun parseReceivedMessage(message: String) {
        val rawMessageType = message.split(" ").first()

        when (rawMessageType) {
            MessageType.USERLIST.toString() -> {
                content.clear()

                val rawMessage = message.split("${MessageType.USERLIST} ").last()
                val userRawMessage: ArrayList<String> = arrayListOf<String>().apply {
                    addAll(rawMessage.split(";").filter { it.isNotEmpty() })
                }

                (0..userRawMessage.size).forEach { _ ->
                    if (userRawMessage.isNotEmpty()) {
                        val userMessage = userRawMessage.first().split(",")

                        if (userMessage.size >= USER_OBJECT_PARAM_COUNT) {
                            val user = User(
                                userMessage[0].toInt(),
                                userMessage[1],
                                userMessage[2],
                                userMessage[3].toDouble(),
                                userMessage[4].toDouble()
                            )

                            content.add(user)
                        }

                        userRawMessage.removeFirst()
                    }
                }

                _users.postValue(content)
            }
            MessageType.UPDATE.toString() -> {
                if (content.isNotEmpty()) {
                    val rawMessage = message.split("${MessageType.UPDATE} ").last()
                    val updateMessage = rawMessage.split(",")

                    if (updateMessage.isNotEmpty() && updateMessage.size >= USER_UPDATE_OBJECT_PARAM_COUNT) {
                        val userId = updateMessage[0].toInt()
                        val userLatitude = updateMessage[1].toDouble()
                        val userLongitude = updateMessage[2].toDouble()
                        val userFound = content.find { it.id == userId }

                        userFound?.let {
                            it.latitude = userLatitude
                            it.longitude = userLongitude
                        }
                    }

                    _userUpdates.postValue(content)
                }
            }
        }
    }
    
    override fun onCleared() {
        tcpClient.stopClient()
        super.onCleared()
    }

    enum class MessageType {
        USERLIST,
        UPDATE
    }

    companion object {
        const val USER_OBJECT_PARAM_COUNT = 5
        const val USER_UPDATE_OBJECT_PARAM_COUNT = 3
    }

}