package com.maartin.socket.features.users

import android.location.Address
import android.location.Geocoder
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.maartin.socket.R
import com.maartin.socket.model.User
import java.util.*

class CustomMarkerInfoViewAdapter(
    private val activity: AppCompatActivity
) : GoogleMap.InfoWindowAdapter {
    private lateinit var userIcon: ImageView
    private lateinit var userUsername: TextView
    private lateinit var userAddress: TextView
    private lateinit var user: User

    override fun getInfoWindow(marker: Marker): View? {
        return null
    }

    override fun getInfoContents(marker: Marker): View {
        val view = activity.layoutInflater.inflate(R.layout.custom_marker_info_view, null)

        user = marker.tag as User
        userIcon = view.findViewById(R.id.userIcon)
        userUsername = view.findViewById(R.id.userUsername)
        userAddress = view.findViewById(R.id.userAddress)

        val userLocation = LatLng(user.latitude, user.longitude)

        setObservables()
        setUserImage(user.imageUrl)
        setUserUsername(user.name)
        setUserAddress(userLocation)

        return view
    }

    private fun setObservables() {
        if (activity is UsersActivity) {
            activity.viewModel.userUpdates.observe(activity, { users ->
                val userToUpdate = users.find { it.id == user.id }

                userToUpdate?.let {
                    val userUpdate = LatLng(it.latitude, it.longitude)
                    setUserAddress(userUpdate)
                }
            })
        }
    }

    private fun setUserImage(imageUrl: String) {
        Glide.with(activity.baseContext)
            .load(imageUrl)
            .placeholder(R.drawable.ic_app_logo)
            .error(R.drawable.ic_app_logo)
            .override(50,50)
            .into(userIcon)
    }

    private fun setUserUsername(username: String) {
        userUsername.text = username
    }

    private fun setUserAddress(userLocation: LatLng) {
        userAddress.text = getAddress(userLocation)
    }

    private fun getAddress(location: LatLng): String {
        val geo = Geocoder(activity, Locale.getDefault())
        val addresses: List<Address>?
        val address: Address?
        val addressText: String

        addresses = geo.getFromLocation(location.latitude, location.longitude, 1)

        if (addresses.isNotEmpty()) {
            address = addresses[0]
            addressText = address.getAddressLine(0)
        } else {
            addressText = activity.getString(R.string.can_not_find_address)
        }

        return addressText
    }

}